package sample;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.Scene;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.collections.*;
import javafx.event.*;
import java.io.*;
import java.net.*;


//Jonathan Umar-Khitab 100520306
//Client Solution file.

public class HttpClient extends Application{

    private BorderPane layout;

    private String currentlySelectedClient = "";    //String that captures which client file item is selected.
    private String currentlySelectedServer = "";    //String that captures which Server File item is selected.
    ObservableList<String> clientFileNames = FXCollections.observableArrayList("File1", "File2", "File3");  //List of Strings containing Client side Files.
    ObservableList<String> serverFileNames = FXCollections.observableArrayList("File1", "File2", "File3");  //List of string containing Server Side Files.

    ListView<String> clientList = new ListView<>(clientFileNames);    //Listview used for the Client's files.
    ListView<String> serverList = new ListView<>(serverFileNames);    //Listview used for the Server's Files.

    public void start(Stage primaryStage) throws  Exception{
        primaryStage.setTitle("JavaFX Demo");

         /* create an edit form (for the bottom of the user interface) */
        GridPane ButtonArea = new GridPane();
        ButtonArea.setPadding(new Insets(10, 10, 10, 10));
        ButtonArea.setVgap(5);
        ButtonArea.setHgap(5);

       //Create the client list and add a listener for user clicks, storing the last item clicked on the client side.
        clientList.setItems(clientFileNames);
        clientList.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                currentlySelectedClient = newValue;
            }
        });

        //Create the Server list and add a listener for user clicks, storing the last item clicked on the client side.
        serverList.setItems(serverFileNames);
        serverList.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                System.out.println(newValue);
                currentlySelectedServer = newValue;
            }
        });

        refreshList();                                                          //Refresh the Client List for the first time.
        refreshServerList();                                                    //Refresh the Server List for the first time.
        Button downloadButton = new Button("Download");                         //Create the Download Button and add the event listener.
        downloadButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                 try{
                    downloadFile(new File("Local/" + currentlySelectedServer)); //Call the download file function by passing the selected file.
                 }
                 catch(Exception except)
                 {
                  except.printStackTrace();
                 }
                refreshList();                                                   //Refresh the List

            }
        });
        ButtonArea.add(downloadButton, 0, 0);

        Button uploadButton = new Button("Upload");                               //Create the Upload button and attach a listener to it.
        uploadButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {

                try {
                    uploadFile(new File("Local/" + currentlySelectedClient));     //Call the upload function passing the selected file.
                } catch (Exception except)
                {
                    except.printStackTrace();
                }
                refreshServerList();                                               //Refresh the server list.

            }
        });
        ButtonArea.add(uploadButton, 1, 0);


        //Place all the items in the layout.
        layout = new BorderPane();
        layout.setRight(serverList);
        layout.setLeft(clientList);
        layout.setTop(ButtonArea);
        Scene scene = new Scene(layout, 400, 400);
        primaryStage.setScene(scene);
        primaryStage.show();

    }

    public static void main(String[] args) {

        launch();   //Launch Scene.

    }

    public void refreshList(){      //Sort through Local File Directory and add the name of each file to the Client List.
        clientList.getItems().clear();
        File file = new File("Local/");
        if (file.isDirectory()) {
            // process all of the files recursively
            File[] filesInDir = file.listFiles();
            for (int i = 0; i < filesInDir.length; i++) {
                clientList.getItems().add(i,filesInDir[i].getName());
            }

        }


    }


    void refreshServerList(){      //Send the DIR command to the server, and receive an updated list of the shared folder contents.

        Socket socket;
        BufferedReader in;
        PrintWriter out;

        String hostName = "";

        int portNumber = 1111;

        try {
            // connect to the server (3-way connection establishment handshake)
            socket = new Socket(hostName, portNumber);

            // wrap the input streams into readers and writers
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new PrintWriter(socket.getOutputStream());


            String request = "DIR";     //Command DIR.
            String delim = "\r\n";
            out.print(request  + delim + delim);
            out.flush();

            // read the response
            String response;


            //Clear the Server list before re-adding all the items to the list again.
            serverList.getItems().clear();
            while ((response = in.readLine()) != null) {        //Cycle through each line of names and add them.
                serverList.getItems().add(response);

            }

            // close the connection (3-way tear down handshake)
            out.close();
            in.close();
            socket.close();
        } catch (IOException e) {
            System.out.println(e);
            e.printStackTrace();
        }
    }

    private String getContentType(String filename) {

            return "text/plain";

        }

    public void uploadFile(File file) throws Exception{     //Upload file to server.
        Socket socket;
        BufferedReader in;
        PrintWriter out;
        String hostName = "";

        BufferedReader transfer = new BufferedReader(new FileReader(file));


        int portNumber = 1111;


        try {
            // connect to the server (3-way connection establishment handshake)
            socket = new Socket(hostName, portNumber);

            // wrap the input streams into readers and writers
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            out = new PrintWriter(socket.getOutputStream());
            String delim = "\r\n";
            out.print("UPLOAD" + delim);                //Send UPLOAD command.
            out.flush();
            out.print(file.getName() + delim);          //Send the file name.
            out.flush();
            String line = transfer.readLine();          //Parse the file and send the contents.
           while(line!=null)
           {
                out.println(line);
               System.out.println(line);
               line=transfer.readLine();
           }
            out.flush();



            // close the connection (3-way tear down handshake)
            out.close();
            in.close();
            socket.close();

        } catch (IOException e) {
            System.out.println(e);
            e.printStackTrace();
        }


    }

    public void downloadFile(File file) throws Exception{
        Socket socket;
        BufferedReader in;
        PrintWriter out;
        String hostName = "";

        System.out.println(file.getName());



        int portNumber = 1111;


        try {
            // connect to the server (3-way connection establishment handshake)
            socket = new Socket(hostName, portNumber);

            // wrap the input streams into readers and writers
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new PrintWriter(socket.getOutputStream());

            String delim = "\r\n";
            out.print("DOWNLOAD" + delim);                       //Send the DOWNLOAD command.
            out.flush();
            out.print(file.getName() + delim);                   //Send the name of the file to download.
            out.flush();



            String newFileName = file.getName();                  //Use the file name when creating the file.
            String line = "";
            line = in.readLine();

            File outputFile = new File("Local/" + newFileName);    //Create the file and fill it with contents being sent from the server.

            if (!outputFile.exists() || outputFile.canWrite()) {
                PrintWriter fout = new PrintWriter(outputFile);
                while(line!=null)
                {   fout.println(line);
                    System.out.println(line);
                    line = in.readLine();

                }
                fout.close();
            }

            // close the connection (3-way tear down handshake)
            out.close();
            in.close();
            socket.close();

        } catch (IOException e) {
            System.out.println(e);
            e.printStackTrace();
        }


    }
}

