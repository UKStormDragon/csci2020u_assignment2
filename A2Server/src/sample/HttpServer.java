package sample;

import java.io.*;
import java.net.*;



public final class HttpServer {
   private ServerSocket serverSocket = null;



   public HttpServer(int port) throws IOException {
      serverSocket = new ServerSocket(port);
   }


   public void handleRequests() throws IOException {
      System.out.println("Server is ready to handle requests.");


      // repeatedly handle incoming requests using threads.
      while(true) {
         Socket socket = serverSocket.accept();
         CommandHandler cHandler = new CommandHandler(socket);          //Create the Command Handler.
         Thread handlerThread = new Thread(cHandler);
         handlerThread.start();

      }
   }

   public static void main(String[] args) {     //Set up the server.
      int port = 1111;
      if (args.length > 0)
         port = Integer.parseInt(args[0]);
      try {
         HttpServer server = new HttpServer(port);
         server.handleRequests();                //Handle requests.
      } catch (IOException e) {
         e.printStackTrace();
      }
   }
}
