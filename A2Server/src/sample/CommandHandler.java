package sample;

import java.io.*;
import java.net.Socket;
import java.util.Date;

/**
 * Created by ukstormdragon on 28/03/16.
 */
public class CommandHandler implements  Runnable {
    private Socket socket = null;
    private BufferedReader requestInput = null;
    private DataOutputStream requestOutput = null;


    public CommandHandler(Socket socket){
        this.socket = socket;
        try {
            requestInput = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            requestOutput = new DataOutputStream(socket.getOutputStream());


        } catch (IOException e) {
            System.err.println("Server Error while processing new socket\r\n");
            e.printStackTrace();
        }

    }




    public void run() {

        try {

            String mainRequestLine = "";
            mainRequestLine = requestInput.readLine();


            //Check the incoming Command and call the appropriate function.
            if(mainRequestLine.equals("DIR"))
            {
                dir();
            }
            if(mainRequestLine.equals("UPLOAD"))
            {
                downloadFromClient();
            }
            if(mainRequestLine.equals("DOWNLOAD"))
            {
                try {
                    uploadToClient();
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                //Close Connections.
                requestInput.close();
                requestOutput.close();
                socket.close();
            } catch (IOException e2) {
            }
        }
    }


    void dir() {        //Git all the files in the shared director and send their names back to the client.

        File file = new File("Shared/");

        if (file.isDirectory()) {

            //Get a list of files.
            File[] filesInDir = file.listFiles();

            PrintWriter outstream = new PrintWriter(requestOutput,true);

            String[] FileNames = new String[filesInDir.length];

            for (int i = 0; i < filesInDir.length; i++) {
                FileNames[i] = filesInDir[i].getName();
                outstream.println(FileNames[i]);
            }


        }
    }

    private void downloadFromClient(){      //Get the name of the file from the client and then create the file and fill it with contents from the client.
        try{
            String downloadedData = requestInput.readLine();

            String newFileName = downloadedData;            //Incoming File Name.

            File outputFile = new File("Shared/" + newFileName);

            downloadedData = requestInput.readLine();

            if (!outputFile.exists() || outputFile.canWrite()) {    //Create file in shared directory.
                PrintWriter fout = new PrintWriter(outputFile);
                while(downloadedData!=null)                         //File Contents.
                {   fout.println(downloadedData);
                    System.out.println(downloadedData);
                    downloadedData = requestInput.readLine();

                }
                fout.close();
            }


        }
       catch(IOException e)
       {
           e.printStackTrace();
       }

    }

    public void uploadToClient() throws Exception{      //Get the file that the client wants and send the contents to them.

        String fileNameInput = requestInput.readLine();
        File file = new File("Shared/" + fileNameInput);        //Get the file.

        BufferedReader transfer = new BufferedReader(new FileReader(file));     //Get the contents of the file.
        PrintWriter outstream = new PrintWriter(requestOutput,true);
        String line = transfer.readLine();
        while(line!=null)                                          //Send the contents of the file back to the client.
        {
            outstream.println(line);
            System.out.println(line);
            line=transfer.readLine();
        }
        outstream.flush();                                          //Clear the stream.

          outstream.close();                                        //Close ths stream.






    }






}